const Sequelize = require("sequelize");
const db = require("../utils/database");

const product_details = db.define('prouct_details', {
    productId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    productName: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false
    },
    productCategory: {
        type: Sequelize.STRING,
        allowNull: false
    },
    productDescription: {
        type: Sequelize.STRING,
        allowNull: true
    }
})


module.exports = product_details
