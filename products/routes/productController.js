const express = require("express");
const Product = require("../model/products");
const productDao = require("../dao/productDao")

const Router = express.Router();

Router.get("/getProductDetails/:productId", (req, res) => {
    productDao.findByProductId(req.params.productId).then(data => {
        console.log("data", data)
        res.send(data)
    }).catch(err => {
        res.status(404).send(err)
    })
})

Router.post("/addProduct", (req, res) => {
    productDao.addProduct(req.body).then(data => {
        res.send({
            message: "Product Addedd Successfully",
            data: data
        })
    }).catch(err => {
        res.status(400).send({
            message: "There is some issue while adding product",
            stackTrace: err
        })
    })
})

module.exports = Router