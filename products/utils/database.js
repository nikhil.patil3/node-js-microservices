const { Sequelize } = require('sequelize');

const db = new Sequelize('products', 'node', 'password', {
    host: 'localhost',
    dialect: 'postgres',
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
    define: {
        timestamps: false
    },
    query: {
        raw: true
    }
})

db.authenticate().then(() => {
    console.log("Connected to db");
}).catch((err) => {
    console.log("Connection Failed", err);
})

module.exports = db;