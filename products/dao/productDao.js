const Product = require("../model/products")

var productDao = {
    addProduct: addProduct,
    findByProductId: findByProductId
}

function addProduct(productData) {
    var product = Product.build(productData);
    return product.save();
}

function findByProductId(id) {
    return Product.findByPk(id)
}

module.exports = productDao