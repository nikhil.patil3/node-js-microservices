const express = require("express");
const bodyParser = require("body-parser");
const productController = require("./routes/productController");
const db = require("./utils/database");

const app = express();

app.use(bodyParser.json());

app.use(productController);

db.sync().then(() => {
    app.listen(4003);
}).catch(err => console.log("Error: " + err));