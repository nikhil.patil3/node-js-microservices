const express = require("express");
const bodyParser = require("body-parser");
const userController = require("./routes/userController");
var morgon = require("morgan");
var logger = require("./utils/logging");
const db = require("./utils/database");

const app = express();

app.use(bodyParser.json());

app.use(morgon('combined', { stream: logger.stream }));

app.use(userController);

db.sync().then(() => {
    app.listen(8080);
}).catch(err => console.log("Error: " + err));