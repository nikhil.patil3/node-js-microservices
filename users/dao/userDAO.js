const User = require("../models/users")

var userDao = {
    findUserByEmail: findUserByEmail,
    create: create,
    findById: findById
}

function create(userData) {
    var user = User.build(userData);
    return user.save();
}

function findUserByEmail(email) {
    return User.findOne({
        where: { email: email }, raw: true
    });
}

function findById(id) {
    return User.findByPk(id)
}

module.exports = userDao