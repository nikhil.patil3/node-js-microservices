const winston = require('winston');
const appRoot = require('app-root-path');

const loggLevel = {
    fatal: 0,
    error: 1,
    warn: 2,
    info: 3,
    debug: 4,
    trace: 5
}
var options = {
    file: {
        level: loggLevel.debug,
        filename: `${appRoot}/logs/app.log`,
        handleExceptions: true,
        json: true,
        maxsize: 5242880, // 5MB
        maxFiles: 5,
        colorize: false,
    },
    console: {
        level: loggLevel.info,
        handleExceptions: true,
        json: false,
        colorize: true,
    },
};

let logger = winston.createLogger({
    transports: [
        new (winston.transports.Console)(options.console),
        new (winston.transports.File)(options.file)
    ],
    exitOnError: false, // do not exit on handled exceptions
});

logger.stream = {
    write: function (message, encoding) {
        logger.debug(message);
    }
}

module.exports = logger