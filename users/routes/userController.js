const express = require("express");
const jwtToken = require("jsonwebtoken");
const axios = require("axios")
const userDao = require("../dao/userDAO");
const logger = require("../utils/logging")

const Router = express.Router();

Router.post("/register", (req, res) => {
    userDao.create(req.body).then((data) => {
        res.send({
            message: "Registration is successfull",
            data: data
        })
    }).catch(err => {
        res.status(400).send(err)
    })
})

Router.post("/login", (req, res) => {
    userDao.findUserByEmail(req.body.email).then(data => {
        var user = data;
        if (user.password === req.body.password) {
            var token = jwtToken.sign({ User: user }, "wowoni")
            res.send({
                message: "Login Successfully",
                token: token
            })
        } else {
            res.status(404).send({
                message: "Bad Credentials"
            })
        }
    }).catch(err => {
        res.status(400).send(err)
    })

});

Router.get("/userDetails", (req, res) => {
    console.log("headers", req.headers.authorization)
    const verifyToken = new Promise((resolve, reject) => {
        const user = jwtToken.verify(req.headers.authorization, "wowoni");
        console.log("user", user.User)
        userDao.findUserByEmail(user.User.email).then(data => {
            resolve(data)
        }).catch(err => {
            reject(err)
        })
    })

    var getOrderData = (userId) => new Promise((resolve, reject) => {
        axios.get("http://127.0.0.1:4002/getOrderDetails/" + userId).then(res => {
            resolve(res.data)
        }).catch(err => {
            reject(err)
        })
    });
    if (req.headers.authorization) {
        verifyToken.then(userData => {
            getOrderData(userData.userid).then(data => {
                logger.debug("orderData", data)
                userData.orderData = data
                res.send({
                    userData
                })
            }).catch(orderErr => {
                console.log("err", err)
                res.send(orderErr)
            })
        }).catch(err => {
            res.status(400).send(err)
        })
    } else {
        res.status(404).send({
            message: "Not allowed user"
        })
    }

});


module.exports = Router;