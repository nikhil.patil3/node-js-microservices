const Sequelize = require("sequelize");
const db = require("../utils/database");

const orders_details = db.define('orders_details', {
    orderId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    userId: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    productId: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    orderDate: {
        type: Sequelize.DATEONLY,
        // set(value) {
        //     this.setDataValue('orderDate', moment(value).format("YYYY-MM-DD"))
        // },
        allowNull: true
    }
})


module.exports = orders_details
