const express = require("express");
const bodyParser = require("body-parser");
const orders = require("./routes/orders.controller");
const db = require("./utils/database");

const app = express();

app.use(bodyParser.json());

app.use(orders);

db.sync().then(() => {
    app.listen(4002);
}).catch(err => console.log("Error: " + err));
