const Order = require("../model/orders")

var orderDao = {
    findOrderByUserId: findOrderByUserId,
    createOrder: createOrder,
    findById: findById
}

function createOrder(orderData) {
    var order = Order.build(orderData);
    return order.save();
}

function findOrderByUserId(id) {
    return Order.findAll({
        where: { userId: id }, raw: true
    });
}

function findById(id) {
    return Order.findByPk(id)
}

module.exports = orderDao