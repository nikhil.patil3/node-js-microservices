const express = require("express");
const orderDao = require("../dao/orderDao")
const axios = require('axios');

const Router = express.Router();

Router.get("/getOrderDetails/:userID", (req, res) => {
    orderDao.findOrderByUserId(req.params.userID).then(data => {
        const orderData = []
        const promises = []
        for (const order of data) {
            promises.push(
                axios.get("http://127.0.0.1:4003/getProductDetails/" + order.productId).then(response => {
                    orderData.push({
                        productName: response.data.productName,
                        productCategory: response.data.productCategory,
                        productDescription: response.data.productDescription,
                        orderDate: order.orderDate
                    })
                })
            )
        }
        Promise.all(promises).then(() => {
            console.log("Product Data", orderData)
            res.send(orderData)
        })
    }).catch(err => {
        res.status(404).send(err)
    })
})

Router.post("/placeOrder", (req, res) => {
    req.body.orderDate = new Date()
    orderDao.createOrder(req.body).then(data => {
        res.send({
            message: "Order Place Successfully",
            data: data
        })
    }).catch(err => {
        res.status(400).send({
            message: "There is some issue for placing order",
            stackTrace: err
        })
    })
})

module.exports = Router